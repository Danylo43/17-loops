"use stric";

//1. Запитайте у користувача два числа. Перевірте, чи є кожне з введених значень числом.
//Якщо ні, то запитуйте у користувача нове занчення до тих пір, поки воно не буде числом.
//Виведіть на екран всі числа від меншого до більшого за допомогою циклу for.
let inputUserFirst;
while (true) {
  inputUserFirst = prompt("Введіть перше число");
  if (
    !isNaN(inputUserFirst) &&
    inputUserFirst.trim() !== "" &&
    inputUserFirst !== null
  ) {
    inputUserFirst = Number(inputUserFirst);
    console.log("Значення корретне");
    break;
  }
  alert("Будь-ласка введіть число");
}

let inputUserSecond;
while (true) {
  inputUserSecond = prompt("Введіть друге число");
  if (
    !isNaN(inputUserSecond) &&
    inputUserSecond.trim() !== "" &&
    inputUserSecond !== null
  ) {
    inputUserSecond = Number(inputUserSecond);
    console.log("Значення корретне");
    break;
  }
  alert("Будь-ласка введіть число");
}

let min = Math.min(inputUserFirst, inputUserSecond);
let max = Math.max(inputUserFirst, inputUserSecond);
console.log(`Ви ввели два числа: ${inputUserFirst} та ${inputUserSecond}`);
console.log(
  `Результат роботи циклу від меньшого (${min}) до більшого (${max}) числа з вашими значеннями: `
);
for (let i = Math.ceil(min); i <= Math.floor(max); i++) {
  console.log(i);
}

// 2. Напишіть програму, яка запитує в користувача число та перевіряє,
//чи воно є парним числом. Якщо введене значення не є парним числом,
//то запитуйте число доки користувач не введе правильне значення.

while (true) {
  let userNumber = prompt("Введіть число");

  if (userNumber !== null && userNumber.trim() !== "") {
    userNumber = Number(userNumber);
    if (!isNaN(userNumber) && userNumber % 2 === 0) {
      console.log(`Ви ввели парне число: ${userNumber}`);
    } else {
      console.log(`Ви ввели НЕ парне число: ${userNumber}`);
    }
  } else {
    console.log(`Некорректне значення`);
  }
}
